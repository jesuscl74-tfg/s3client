--liquibase formatted sql
--changeset jesuscl74:01-init-database-20230321
create table user_data
(
    id            uuid         not null
        constraint pk_user_data
            primary key,
    full_name     varchar(255) not null,
    email         varchar(255) not null
        constraint ak_user_data_email
            unique,
    password      varchar(255) not null,
    registered_on timestamp    not null,
    last_updated  timestamp,
    version smallint default 0 not null
);

insert into user_data(id, full_name, email, password, registered_on, last_updated, version)
values ('c42bbf5e-9521-45b5-86ed-2bb9692d4f3e', 'Jesús Calvo López (UOC)', 'jesuscl74@uoc.edu', '1234', current_timestamp, current_timestamp, 1);
insert into user_data(id, full_name, email, password, registered_on, last_updated, version)
values ('489841da-44ba-487f-bd7a-8bdccabcb2db', 'Jesús Calvo López (Personal)', 'jesus.calvo@gmail.com', '1234', current_timestamp, current_timestamp, 1);

create table connection
(
    user_id         uuid          not null,
    bucket_name     varchar(256)  not null,
    description     varchar(4096),
    endpoint        varchar(2048) not null,
    access_id       varchar(256)  not null,
    access_password varchar(256)  not null,
    created_on      timestamp     not null,
    last_updated    timestamp
);

alter table connection
    add constraint fk_connection_user_id
        foreign key (user_id) references user_data;

create unique index ixu_connection_user_bucket
    on connection (user_id, bucket_name);

insert into connection(user_id, bucket_name, description, endpoint, access_id, access_password, created_on, last_updated)
values ('c42bbf5e-9521-45b5-86ed-2bb9692d4f3e', 'minio-s3client-test',
        'Instancia MinIO para realizar pruebas ejecutandose en un contenedor docker', 'http://localhost:9000',
        'minioadmin', 'minioadmin', current_timestamp, current_timestamp);


insert into connection(user_id, bucket_name, description, endpoint, access_id, access_password, created_on, last_updated)
values ('c42bbf5e-9521-45b5-86ed-2bb9692d4f3e', 'minio-s3client-test-2',
        'Instancia MinIO para realizar pruebas ejecutandose en un contenedor docker', 'http://localhost:9002',
        'minioadmin', 'minioadmin', current_timestamp, current_timestamp);

--changeset jesuscl74:02-remove-passwords-20230529

alter table user_data
    drop column password;

