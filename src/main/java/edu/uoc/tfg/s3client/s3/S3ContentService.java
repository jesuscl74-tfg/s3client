package edu.uoc.tfg.s3client.s3;

import edu.uoc.tfg.s3client.domain.*;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import software.amazon.awssdk.core.ResponseInputStream;
import software.amazon.awssdk.core.async.AsyncRequestBody;
import software.amazon.awssdk.core.async.AsyncResponseTransformer;
import software.amazon.awssdk.core.async.BlockingInputStreamAsyncRequestBody;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.*;
import software.amazon.awssdk.transfer.s3.S3TransferManager;
import software.amazon.awssdk.transfer.s3.model.*;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

@Slf4j
@Service
public class S3ContentService implements ContentService {
    private final S3Adapter s3Adapter;

    public S3ContentService(S3Adapter s3Adapter) {
        this.s3Adapter = s3Adapter;
    }


    @Override
    public Set<ContentObject> listContent(@NonNull Connection connection) {
        return listContent(connection, null);
    }

    @Override
    public Set<ContentObject> listContent(@NonNull Connection connection, Folder folder) {
        String prefix = folder == null ? "" : folder.name();
        log.debug("Requesting to list content for connection for bucket {} and folder {}", connection.bucketName(), prefix);
        ListObjectsV2Response response;
        try (S3Client s3 = s3Adapter.connect(connection)) {
            ListObjectsV2Request request = ListObjectsV2Request.builder()
                    .bucket(connection.bucketName())
                    .delimiter("/")
                    .prefix(prefix)
                    .build();
            response = s3.listObjectsV2(request);
        } catch (S3Exception e) {
            log.error("Error while retrieving content from bucket {} and folder {}", connection.bucketName(), prefix, e);
            return Collections.emptySet();
        }

        if (!response.hasContents() && !response.hasCommonPrefixes()) {
            log.info("No content found");
            return Collections.emptySet();
        }
        List<S3Object> objects = response.contents();
        log.debug("Retrieved {} objects from S3 repo", objects.size());
        SortedSet<ContentObject> content = new TreeSet<>();
        List<CommonPrefix> commonPrefixes = response.commonPrefixes();
        commonPrefixes.forEach(commonPrefix -> {
            log.debug("prefix: {}", commonPrefix.prefix());
            Folder folderFound = new Folder(commonPrefix.prefix());
            content.add(folderFound);
        });
        objects.forEach(s3Object -> {
            log.debug("object: {}", s3Object.key());
            if (folder != null && s3Object.key().equals(folder.name())) {
                log.warn("The object retrieved represents the current folder, we skip it.");
            } else {
                File file = new File(s3Object.key(), s3Object.size(), LocalDateTime.ofInstant(s3Object.lastModified(), ZoneId.systemDefault()));
                content.add(file);
            }
        });
        return content;
    }


    @Override
    public String createFolder(@NonNull Connection connection, @NonNull String folderName) {
        return createFolder(connection, folderName, null);
    }

    @Override
    public String createFolder(@NonNull Connection connection, @NonNull String folderName, Folder parentFolder) {
        String key = buildKey(folderName, parentFolder);
        try (S3Client s3 = s3Adapter.connect(connection)) {
            s3.putObject(PutObjectRequest.builder()
                            .bucket(connection.bucketName())
                            .key(key)
                            .build(),
                    RequestBody.empty());
            log.info("Folder {} created in bucket {}", key, connection.bucketName());
            return key;
        } catch (S3Exception e) {
            log.error("Error while creating in bucket {} empty object {}", connection.bucketName(), key, e);
            return null;
        }
    }

    @Override
    public Resource downloadFile(@NonNull Connection connection, @NonNull String path) {
        S3TransferManager transferManager = s3Adapter.transferManager(connection);
        DownloadRequest<ResponseInputStream<GetObjectResponse>> downloadRequest =
                DownloadRequest.builder()
                        .getObjectRequest(b -> b.bucket(connection.bucketName()).key(path))
                        .responseTransformer(AsyncResponseTransformer.toBlockingInputStream())
                        .build();

        // Initiate the transfer
        Download<ResponseInputStream<GetObjectResponse>> download =
                transferManager.download(downloadRequest);
        // Wait for the transfer to complete
        CompletedDownload<ResponseInputStream<GetObjectResponse>> completed = download.completionFuture().join();
        return new InputStreamResource(completed.result());
    }

    @Override
    public void uploadFile(@NonNull Connection connection, String folderPath, @NonNull MultipartFile multipartFile) {
        try {
            uploadFile(connection, folderPath, multipartFile.getOriginalFilename(), multipartFile.getInputStream(), multipartFile.getSize());
        } catch (IOException e) {
            throw new IllegalStateException(String.format("There was an error while trying to upload file %s", multipartFile.getOriginalFilename()), e);
        }
    }
    @Override
    public void uploadFile(@NonNull Connection connection, String folderPath, String filename, @NonNull InputStream inputStream, long contentSize) {
        S3Client s3Client = s3Adapter.connect(connection);
        String path = buildKey(filename, folderPath);
        log.debug("We are going to upload to bucket {} the file {}", connection.bucketName(), path);
        PutObjectRequest request = PutObjectRequest.builder()
                .bucket(connection.bucketName())
                .key(path)
                .build();
        RequestBody body = RequestBody.fromInputStream(inputStream, contentSize);
        s3Client.putObject(request, body);
        log.info("Finished upload to bucket {} of the file {}", connection.bucketName(), path);
    }

    @Override
    public void deleteFile(Connection connection, String path) {
        S3Client s3Client = s3Adapter.connect(connection);
        log.debug("We are going to delete from bucket {} the file {}", connection.bucketName(), path);
        DeleteObjectRequest deleteObjectRequest = DeleteObjectRequest.builder()
                .bucket(connection.bucketName())
                .key(path)
                .build();
        s3Client.deleteObject(deleteObjectRequest);
    }

    public void uploadFileWithTransferManager(@NonNull Connection connection, String folderPath, String filename, @NonNull InputStream inputStream, long contentSize) {
        S3TransferManager transferManager = s3Adapter.transferManager(connection);
        String path = buildKey(filename, folderPath);
        log.debug("We are going to upload to bucket {} the file {}", connection.bucketName(), path);
        BlockingInputStreamAsyncRequestBody body = AsyncRequestBody.forBlockingInputStream(contentSize);
        UploadRequest uploadRequest = UploadRequest.builder()
                .requestBody(body)
                .putObjectRequest(request -> request.bucket(connection.bucketName())
                        .key(path))
                .build();
        Upload upload = transferManager.upload(uploadRequest);
        body.writeInputStream(inputStream);
        // Wait for the upload to complete
        upload.completionFuture().join();
        log.info("Finished upload to bucket {} of the file {}", connection.bucketName(), path);
    }

    private String buildKey(String filename, String path) {
        StringBuilder kb = new StringBuilder();
        if (path != null) {
            kb.append(path);
            if (!path.endsWith("/")) {
                kb.append("/");
            }
        }
        kb.append(filename);
        return kb.toString();
    }

    private String buildKey(String folderName, Folder parentFolder) {
        StringBuilder kb = new StringBuilder();
        if (parentFolder != null) {
            kb.append(parentFolder.name());
            if (!parentFolder.name().endsWith("/")) {
                kb.append("/");
            }
        }
        kb.append(folderName);
        if (!folderName.endsWith("/")) {
            kb.append("/");
        }
        return kb.toString();
    }
}
