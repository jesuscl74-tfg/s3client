package edu.uoc.tfg.s3client.s3;

import edu.uoc.tfg.s3client.domain.Connection;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.http.apache.ApacheHttpClient;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3AsyncClient;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.transfer.s3.S3TransferManager;

import java.net.URI;
import java.time.Duration;

@Slf4j
@Service
public class S3Adapter {
    public S3Client connect(Connection connection) {
        return S3Client.builder()
                .httpClientBuilder(ApacheHttpClient.builder()
                        .maxConnections(100)
                        .connectionTimeout(Duration.ofSeconds(10L)))
                .region(Region.EU_WEST_1)
                .endpointOverride(URI.create(connection.endpoint()))
                .forcePathStyle(true)
                .credentialsProvider(
                        StaticCredentialsProvider.create(
                                AwsBasicCredentials.create(
                                        connection.accessId(),
                                        connection.accessPassword())))
                .build();
    }

    public S3AsyncClient connectAsync(Connection connection) {
        return S3AsyncClient.builder()
                .region(Region.EU_WEST_1)
                .endpointOverride(URI.create(connection.endpoint()))
                .forcePathStyle(true)
                .credentialsProvider(
                        StaticCredentialsProvider.create(
                                AwsBasicCredentials.create(
                                        connection.accessId(),
                                        connection.accessPassword())))
                .build();
    }

    public S3TransferManager transferManager(Connection connection) {
        return S3TransferManager.builder()
                .s3Client(connectAsync(connection))
                .build();
    }
}
