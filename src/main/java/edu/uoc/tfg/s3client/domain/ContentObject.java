package edu.uoc.tfg.s3client.domain;

import java.time.LocalDateTime;

public interface ContentObject extends Comparable<ContentObject> {
    String name();
    Long size();

    LocalDateTime lastUpdated();

    default boolean isFolder() {
        return this.size() == 0;
    }

    default int compareTo(ContentObject other) {
        if ((this.isFolder() && other.isFolder()) || (!this.isFolder() && !other.isFolder())) {
            // both elements are folders or both elements are Files
            return this.name().compareTo(other.name());
        }
        // the first element is a folder, and the second is a file or vice-versa
        return  this.isFolder() ? -1 : 1;
    }
}
