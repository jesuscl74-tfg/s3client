package edu.uoc.tfg.s3client.domain;

import org.springframework.data.relational.core.mapping.InsertOnlyProperty;

import java.time.LocalDateTime;

public record Connection(
        String description,
        String bucketName,
        String endpoint,
        String accessId,
        String accessPassword,

        @InsertOnlyProperty
        LocalDateTime createdOn,
        LocalDateTime lastUpdated) {
}
