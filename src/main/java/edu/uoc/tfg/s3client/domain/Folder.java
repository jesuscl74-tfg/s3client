package edu.uoc.tfg.s3client.domain;

import java.time.LocalDateTime;

public record Folder(String name) implements ContentObject {
    @Override
    public Long size() {
        return 0L;
    }

    @Override
    public LocalDateTime lastUpdated() {
        return null;
    }
}
