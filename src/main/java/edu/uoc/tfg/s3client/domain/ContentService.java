package edu.uoc.tfg.s3client.domain;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.Set;

public interface ContentService {
    Set<ContentObject> listContent(Connection connection);
    Set<ContentObject> listContent(Connection connection, Folder folder);

    String createFolder(Connection connection, String folderName);
    String createFolder(Connection connection, String folderName, Folder parentFolder );

    Resource downloadFile(Connection connection, String path);

    void uploadFile(Connection connection, String folderPath, MultipartFile multipartFile);
    void uploadFile(Connection connection, String folderPath, String filename, InputStream inputStream, long contentSize);
    void deleteFile(Connection connection, String path);
}
