package edu.uoc.tfg.s3client.domain;

import java.time.LocalDateTime;

public record File(String name, Long size, LocalDateTime lastUpdated) implements ContentObject {
}
