package edu.uoc.tfg.s3client.domain;

import lombok.NonNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.relational.core.mapping.MappedCollection;
import org.springframework.data.relational.core.mapping.Table;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Table("user_data")
public record User(
        @Id
        UUID id,
        String fullName,
        String email,
        LocalDateTime registeredOn,
        LocalDateTime lastUpdated,
        @Version
        int version,
        @MappedCollection(idColumn = "user_id")
        Set<Connection> connections) {

    public void validate() {
        boolean isNotValid = false;
        StringBuilder sb = new StringBuilder();
        if (id() == null) {
            isNotValid = true;
            sb.append("The ID is mandatory. ");
        }
        if (email() == null || email().isBlank()) {
            isNotValid = true;
            sb.append("The email is mandatory. ");
        }
        if (fullName() == null || fullName().isBlank()) {
            isNotValid = true;
            sb.append("The name is mandatory. ");
        }
        if (isNotValid) {
            throw new IllegalStateException(sb.toString());
        }
    }

    public Optional<Connection> findConnectionByBucketName(@NonNull String bucketName) {
        if (connections() == null || connections().isEmpty()) {
            return Optional.empty();
        }
        return connections().stream()
                .filter(conn -> bucketName.equalsIgnoreCase(conn.bucketName()))
                .findFirst();
    }

    public User copyWithNewId() {
        LocalDateTime now = LocalDateTime.now();
        return new User(UUID.randomUUID(), fullName(), email(), Optional.ofNullable(registeredOn()).orElse(now), now, version(), connections());
    }
}
