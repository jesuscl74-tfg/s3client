package edu.uoc.tfg.s3client.domain;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UserService {
    Optional<User> findById(UUID id);
    Optional<User> findByEmail(String email);
    List<User> findAll();

    UUID create(User user);
    void update(User user);
    void delete(UUID id);

}
