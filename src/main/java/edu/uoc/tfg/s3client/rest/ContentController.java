package edu.uoc.tfg.s3client.rest;

import edu.uoc.tfg.s3client.domain.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.nio.file.Paths;
import java.util.Set;
import java.util.UUID;

import static java.lang.String.format;

@Slf4j
@CrossOrigin
@RestController
@RequestMapping("content")
public class ContentController {
    private final ContentService contentService;
    private final UserService userService;

    public ContentController(ContentService contentService, UserService userService) {
        this.contentService = contentService;
        this.userService = userService;
    }

    @GetMapping("/user/{userId}/bucket/{bucketName}")
    @ResponseStatus(HttpStatus.OK)
    public Set<ContentObject> listContent(
            @PathVariable UUID userId,
            @PathVariable String bucketName,
            @RequestParam(value = "folder", required = false) String folderPath) {
        Connection connection = getConnection(userId, bucketName);
        Folder folder = folderPath == null ? null : new Folder(folderPath);
        return contentService.listContent(connection, folder);
    }

    @PostMapping("/user/{userId}/bucket/{bucketName}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<Void> createFolder(
            @PathVariable UUID userId,
            @PathVariable String bucketName,
            @RequestBody CreateFolderCommand createFolderCommand) {
        Connection connection = getConnection(userId, bucketName);
        String keyCreated = contentService.createFolder(connection, createFolderCommand.newFolder().name(), createFolderCommand.parent());
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                .queryParam("folder", keyCreated)
                .build()
                .toUri();
        return ResponseEntity.created(uri).build();
    }

    private Connection getConnection(UUID userId, String bucketName) {
        User user = userService.findById(userId)
                .orElseThrow(() -> new ResponseStatusException(
                        HttpStatus.NOT_FOUND,
                        format("There is no user with ID %s", userId)));
        return user.findConnectionByBucketName(bucketName)
                .orElseThrow(() -> new ResponseStatusException(
                        HttpStatus.NOT_FOUND,
                        format("There is no connection with bucket name %s for user with ID %s", bucketName, userId)));
    }

    @GetMapping(path = "/user/{userId}/bucket/{bucketName}/content")
    public ResponseEntity<Resource> download(@PathVariable UUID userId,
                                             @PathVariable String bucketName,
                                             @RequestParam("path") String path) {
        log.debug("Trying to download file {} from bucket {}", path, bucketName);
        String filename = Paths.get(path).getFileName().toString();
        Connection connection = getConnection(userId, bucketName);
        Resource resource = contentService.downloadFile(connection, path);
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, String.format("attachment; filename=\"%s\"", filename));
        return ResponseEntity.ok()
                .headers(headers)
                //.contentLength(resource.contentLength())
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(resource);
    }

    @PostMapping(path = "/user/{userId}/bucket/{bucketName}/content")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Void> upload(@PathVariable UUID userId,
                       @PathVariable String bucketName,
                       @RequestParam(value = "path", required = false) String path,
                       @RequestParam("file") MultipartFile file) {
        Connection connection = getConnection(userId, bucketName);
        contentService.uploadFile(connection, path, file);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                .replaceQueryParam("path", path == null ? file.getOriginalFilename() : String.format("%s/%s", path, file.getOriginalFilename()))
                .build()
                .toUri();
        return ResponseEntity.created(uri).build();
    }

    @DeleteMapping(path = "/user/{userId}/bucket/{bucketName}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<Void> delete(@PathVariable UUID userId,
                                       @PathVariable String bucketName,
                                       @RequestParam("path") String path) {
        Connection connection = getConnection(userId, bucketName);
        contentService.deleteFile(connection, path);
        return ResponseEntity.noContent().build();
    }

}
