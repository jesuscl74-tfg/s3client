package edu.uoc.tfg.s3client.rest;

import edu.uoc.tfg.s3client.domain.Folder;

public record CreateFolderCommand(Folder newFolder, Folder parent) {
}
