package edu.uoc.tfg.s3client.rest;

import edu.uoc.tfg.s3client.domain.User;
import edu.uoc.tfg.s3client.domain.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.UUID;

import static java.lang.String.format;

@Slf4j
@CrossOrigin
@RestController
@RequestMapping("users")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public List<User> findUsers(@RequestParam(value = "email", required = false) String email) {
        log.debug("Listing users. Email = {}", email);
        if (email == null) {
            return userService.findAll();
        }
        return userService.findByEmail(email)
                .stream()
                .toList();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<User> findById(@PathVariable UUID id) {
        log.debug("find user by ID {}", id);
        return userService.findById(id).map(category -> ResponseEntity.ok().body(category))
                .orElse(ResponseEntity.notFound().build());
    }


    @CrossOrigin(exposedHeaders = "*")
    @PostMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<Void> create(@RequestBody User user) {
        log.debug("create user {}", user);
        try {
            UUID newId = userService.create(user);
            URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                    .path("/{id}")
                    .buildAndExpand(newId)
                    .toUri();
            return ResponseEntity.created(uri).build();
        } catch (IllegalStateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@PathVariable("id") UUID userId, @RequestBody User user) {
        // First, we validate that the category ID in the URL path exists
        if (userService.findById(userId).isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, format("There is no user with ID %s", userId));
        }
        // We check if the payload sent in the body includes the ID, and in that case we validate that is the same as the one from the URL
        if (user.id() != null && !userId.equals(user.id())) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    format("The user sent in the body of the request contain an ID with value %s, " +
                                    "but in the url we used the id %s which does not match.",
                            user.id(), userId));
        }
        try {
            userService.update(user);
        } catch (IllegalStateException e) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") UUID userId) {
        // First, we validate that the category ID in the URL path exists
        if (userService.findById(userId).isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, format("There is no user with ID %s", userId));
        }
        try {
            userService.delete(userId);
        } catch (IllegalStateException e) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
