package edu.uoc.tfg.s3client.db;

import edu.uoc.tfg.s3client.domain.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends CrudRepository<User, UUID> {
    Optional<User> findFirstByEmail(String email);
}
