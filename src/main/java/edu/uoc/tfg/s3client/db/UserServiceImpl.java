package edu.uoc.tfg.s3client.db;

import edu.uoc.tfg.s3client.domain.User;
import edu.uoc.tfg.s3client.domain.UserService;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.StreamSupport;

@Slf4j
@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Optional<User> findById(@NonNull UUID id) {
        log.debug("Looking for user with ID {}", id);
        return userRepository.findById(id);
    }

    @Override
    public Optional<User> findByEmail(@NonNull String email) {
        log.debug("Looking for user with email {}", email);
        return userRepository.findFirstByEmail(email);
    }

    @Override
    public List<User> findAll() {
        log.debug("Getting all users.");
        return StreamSupport.stream(userRepository.findAll().spliterator(), false)
                .toList();
    }

    @Override
    public UUID create(@NonNull User user) {
        log.debug("Saving new user {}", user);
        User userWithId = user.id() == null ? user.copyWithNewId() : user;
        userWithId.validate();
        validateUserIdDoesNotExist(userWithId);
        validateUserEmailDoesNotExist(userWithId);
        userRepository.save(userWithId);
        return userWithId.id();
    }

    private void validateUserEmailDoesNotExist(User user) {
        if (userRepository.findFirstByEmail(user.email()).isPresent()) {
            throw new IllegalStateException(String.format("The user with email %s already exists", user.email()));
        }
    }

    private void validateUserIdDoesNotExist(User user) {
        if (userRepository.existsById(user.id())) {
            throw new IllegalStateException(String.format("The user with ID %s already exists", user.id()));
        }
    }

    @Override
    public void update(User user) {
        log.debug("Saving existing user {}", user);
        user.validate();
        User existingUser = findById(user.id())
                .orElseThrow(() -> new IllegalStateException(String.format("The user with ID %s does not exist", user.id())));
        if (!existingUser.email().equalsIgnoreCase(user.email())) {
            validateUserEmailDoesNotExist(user);
        }
        userRepository.save(user);
    }

    @Override
    public void delete(UUID id) {
        log.debug("Deleting existing user with ID {}", id);
        User existingUser = findById(id)
                .orElseThrow(() -> new IllegalStateException(String.format("The user with ID %s does not exist", id)));
        userRepository.delete(existingUser);
    }
}
