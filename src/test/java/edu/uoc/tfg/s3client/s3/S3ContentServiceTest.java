package edu.uoc.tfg.s3client.s3;

import edu.uoc.tfg.s3client.domain.Connection;
import edu.uoc.tfg.s3client.domain.ContentObject;
import edu.uoc.tfg.s3client.domain.ContentService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
@Slf4j
class S3ContentServiceTest {
    private static final Connection LOCAL_CONNECTION = new Connection("test1",
            "minio-s3client-test",
            "http://localhost:9000",
            "minioadmin",
            "minioadmin",
            LocalDateTime.now(),
            LocalDateTime.now());
    @Autowired
    private ContentService contentService;

    @Test
    void listContent() {
    }

    @Test
    void testListContent() {
    }

    @Test
    void createFolder() {
    }

    @Test
    void testCreateFolder() {

    }

    @Test
    void download_file_when_the_file_exists_works() {
        Resource r = contentService.downloadFile(LOCAL_CONNECTION, "tfg/tfg.mpp");
        assertThat(r).isNotNull()
                .matches(Resource::exists);
    }

    @Test
    void upload_single_file_works() throws IOException {
        Path path = Paths.get("src/test/resources/downloads/pec1.pdf");
        File file = path.toFile();
        contentService.uploadFile(LOCAL_CONNECTION, null, "pec1.pdf", Files.newInputStream(path), file.length());
        Set<ContentObject> content = contentService.listContent(LOCAL_CONNECTION);
        assertThat(content).isNotEmpty()
                .anyMatch(resultFile -> "pec1.pdf".equals(resultFile.name()));
    }

    @Test
    void delete_single_file_works() throws IOException {
        // First we upload the file to ensure that it is there
        Path path = Paths.get("src/test/resources/downloads/to-delete.txt");
        File file = path.toFile();
        contentService.uploadFile(LOCAL_CONNECTION, null, "to-delete.txt", Files.newInputStream(path), file.length());
        Set<ContentObject> contentBeforeDelete = contentService.listContent(LOCAL_CONNECTION);
        assertThat(contentBeforeDelete).isNotEmpty()
                .anyMatch(resultFile -> "to-delete.txt".equals(resultFile.name()));
        // Now we delete it
        contentService.deleteFile(LOCAL_CONNECTION, "to-delete.txt");
        // Check that it is not there anymore
        Set<ContentObject> contentAfterDelete = contentService.listContent(LOCAL_CONNECTION);
        assertThat(contentAfterDelete).isNotEmpty()
                .noneMatch(resultFile -> "to-delete.txt".equals(resultFile.name()));
    }

}