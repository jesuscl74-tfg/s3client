package edu.uoc.tfg.s3client.s3;

import edu.uoc.tfg.s3client.domain.Connection;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;

@SpringBootTest
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
@Slf4j
class S3AdapterTest {
    @Autowired
    private S3Adapter s3Adapter;

    @Test
    void given_two_different_connections_we_can_connect_use_both_of_them() {
        Connection c1 = new Connection("test1",
                "1",
                "http://localhost:9000",
                "minioadmin",
                "minioadmin",
                LocalDateTime.now(),
                LocalDateTime.now());
        Connection c2 = new Connection("test2",
                "2",
                "http://localhost:9002",
                "minioadmin",
                "minioadmin",
                LocalDateTime.now(),
                LocalDateTime.now());

        s3Adapter.connect(c1);
        s3Adapter.connect(c2);
    }
}