package edu.uoc.tfg.s3client.domain;

import edu.uoc.tfg.s3client.db.UserRepository;
import edu.uoc.tfg.s3client.db.UserServiceImpl;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@SpringJUnitConfig
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class UserServiceTest {
    private static final UUID EXISTING_ID = UUID.fromString("c42bbf5e-9521-45b5-86ed-2bb9692d4f3e");
    private static final UUID NON_EXISTING_ID = UUID.randomUUID();

    @TestConfiguration
    static class UserServiceTestConfig {
        @Bean
        public UserService userService(UserRepository repository) {
            return new UserServiceImpl(repository);
        }
    }

    @MockBean
    private UserRepository userRepository;
    @Autowired
    private UserService userService;

    @Test
    void given_existing_id_when_calling_findById_returns_user() {
        Mockito.when(userRepository.findById(EXISTING_ID)).thenReturn(Optional.of(new User(
                EXISTING_ID,
                "Name1",
                "user@domain.com",
                LocalDateTime.of(2023, 4, 1, 15, 0),
                LocalDateTime.now(),
                1,
                Collections.emptySet()
        )));
        Optional<User> userFound = userService.findById(EXISTING_ID);
        assertThat(userFound).isPresent()
                .hasValueSatisfying(user -> {
                    assertThat(user.id()).isEqualTo(EXISTING_ID);
                    assertThat(user.fullName()).isEqualTo("Name1");
                });

        Mockito.verify(userRepository, VerificationModeFactory.times(1)).findById(Mockito.any());
        Mockito.reset(userRepository);
    }

    @Test
    void given_existing_email_when_calling_findByEmail_return_user() {
        String existingEmail = "user@domain.com";
        Mockito.when(userRepository.findFirstByEmail(existingEmail)).thenReturn(Optional.of(new User(
                EXISTING_ID,
                "Name1",
                existingEmail,
                LocalDateTime.of(2023, 4, 1, 15, 0),
                LocalDateTime.now(),
                1,
                Collections.emptySet()
        )));
        Optional<User> userFound = userService.findByEmail(existingEmail);
        assertThat(userFound).isPresent()
                .hasValueSatisfying(user -> {
                    assertThat(user.id()).isEqualTo(EXISTING_ID);
                    assertThat(user.fullName()).isEqualTo("Name1");
                });

        Mockito.verify(userRepository, VerificationModeFactory.times(1)).findFirstByEmail(Mockito.anyString());
        Mockito.reset(userRepository);
    }

    @Test
    void given_there_are_users_when_calling_findAll_return_users() {
        List<User> existingUsers = List.of(
                new User(
                        UUID.randomUUID(),
                        "Name1",
                        "user1@domain.com",
                        LocalDateTime.of(2023, 4, 1, 15, 0),
                        LocalDateTime.now(),
                        1,
                        Collections.emptySet()),
                new User(
                        UUID.randomUUID(),
                        "Name2",
                        "user2@domain.com",
                        LocalDateTime.of(2023, 4, 1, 15, 0),
                        LocalDateTime.now(),
                        1,
                        Collections.emptySet())
        );
        Mockito.when(userRepository.findAll()).thenReturn(existingUsers);
        List<User> usersFound = userService.findAll();
        assertThat(usersFound).isNotEmpty()
                .containsAll(existingUsers);

        Mockito.verify(userRepository, VerificationModeFactory.times(1)).findAll();
        Mockito.reset(userRepository);
    }
}