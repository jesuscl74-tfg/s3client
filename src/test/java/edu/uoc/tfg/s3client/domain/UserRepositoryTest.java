package edu.uoc.tfg.s3client.domain;

import edu.uoc.tfg.s3client.db.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
@Slf4j
class UserRepositoryTest {
    @Autowired
    private UserRepository repository;

    @Test
    void given_existing_connection_id_when_finding_we_get_data() {
        log.debug("We are going to run the query");
        Optional<User> user = repository.findById(UUID.fromString("c42bbf5e-9521-45b5-86ed-2bb9692d4f3e"));
        assertThat(user).isPresent()
                .hasValueSatisfying(u -> {
                    assertThat(u.id()).isNotNull()
                            .isEqualTo(UUID.fromString("c42bbf5e-9521-45b5-86ed-2bb9692d4f3e"));
                    assertThat(u.fullName()).isNotNull()
                        .isEqualTo("Jesús Calvo López (UOC)");
                    assertThat(u.email()).isNotNull()
                            .isEqualTo("jesuscl74@uoc.edu");
                    assertThat(u.connections()).isNotNull()
                            .isNotEmpty()
                            .hasSizeGreaterThan(1);
                });
        log.debug("The test has finished, this is the object retrieved: {}", user.get());
    }

    @Test
    void given_existing_email_when_finding_we_get_data() {
        log.debug("We are going to run the query");
        Optional<User> user = repository.findFirstByEmail("jesuscl74@uoc.edu");
        assertThat(user).isPresent()
                .hasValueSatisfying(u -> {
                    assertThat(u.id()).isNotNull()
                            .isEqualTo(UUID.fromString("c42bbf5e-9521-45b5-86ed-2bb9692d4f3e"));
                    assertThat(u.fullName()).isNotNull()
                            .isEqualTo("Jesús Calvo López (UOC)");
                    assertThat(u.email()).isNotNull()
                            .isEqualTo("jesuscl74@uoc.edu");
                    assertThat(u.connections()).isNotNull()
                            .isNotEmpty()
                            .hasSizeGreaterThan(1);
                });
        log.debug("The test has finished, this is the object retrieved: {}", user.get());
    }

}