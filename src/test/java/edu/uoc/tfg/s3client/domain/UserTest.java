package edu.uoc.tfg.s3client.domain;

import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;

import java.util.Set;
import java.util.UUID;

import static org.assertj.core.api.Assertions.*;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class UserTest {
    @Test
    void given_an_user_with_id_and_non_empty_mandatory_fields_validate_does_not_throw_exception() {
        User user = new User(UUID.randomUUID(), "test name", "test@email.eu",  null, null, 0, null);
        assertThatNoException().isThrownBy(user::validate);
    }

    @Test
    void given_an_user_without_id_and_non_empty_mandatory_fields_validate_throws_an_exception() {
        User user = new User(null, "test name", "test@email.eu", null, null, 0, null);
        assertThatThrownBy(user::validate)
                .isInstanceOf(IllegalStateException.class)
                .hasMessageContaining("The ID is mandatory");
    }

    @Test
    void given_an_user_without_id_and_empty_mandatory_fields_validate_throws_an_exception() {
        User user = new User(null, null, null, null, null, 0, null);
        assertThatThrownBy(user::validate)
                .isInstanceOf(IllegalStateException.class)
                .hasMessageContaining("The ID is mandatory")
                .hasMessageContaining("The email is mandatory")
                .hasMessageContaining("The name is mandatory");
    }

    @Test
    void given_an_user_with_several_connections_when_calling_find_by_and_existing_bucket_name_returns_a_connection() {
        User user = new User(null, null, null, null, null, 0, Set.of(
                new Connection("conn 1", "bucket1", null, null, null, null, null),
                new Connection("conn 2", "bucket2", null, null, null, null, null),
                new Connection("conn 3", "bucket3", null, null, null, null, null)
        ));
        assertThat(user.findConnectionByBucketName("bucket3")).isPresent()
                .hasValueSatisfying(conn -> {
                    assertThat(conn.bucketName()).isEqualTo("bucket3");
                    assertThat(conn.description()).isEqualTo("conn 3");
                });
    }

}