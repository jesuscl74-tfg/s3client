FROM eclipse-temurin:17-jre-alpine AS build
VOLUME /tmp
ARG EXTRACTED=/build/extracted
COPY ${EXTRACTED}/dependencies/BOOT-INF/lib /app/lib
COPY ${EXTRACTED}/application/META-INF /app/META-INF
COPY ${EXTRACTED}/application/BOOT-INF/classes /app
ENTRYPOINT ["java","-cp","app:app/lib/*","edu.uoc.tfg.s3client.S3ClientApplication"]