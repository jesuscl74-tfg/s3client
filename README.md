# S3 Web Client REST API
This is a REST service for interacting with S3 platforms. It contains a catalog of connections to S3 buckets.

## Running the application
From the project root we can run the following

```
gradlew bootRun
```

---

## Technologies used
- Java 17
- [AWS SDK for Java](https://docs.aws.amazon.com/sdk-for-java/latest/developer-guide/home.html)
- [JUnit 5](https://junit.org/junit5/docs/current/user-guide/)
- [AsserJ](https://assertj.github.io/doc/)
- [ArchUnit](https://www.archunit.org/userguide/html/000_Index.html)
- [Lombok](https://projectlombok.org/features/all)
- [MinIO](https://docs.min.io/minio/baremetal/): We use it as an AWS S3 implementation for local development

For installing Java 17, we can use SDKMAN. I have installed the Temurin distribution:
```
sdk install java 17.0.6-tem
```

## Local environment for S3
For testing Amazon S3 we are using MinIO. We are using docker and there is a docker-compose configuration file. From the 
command line, from the root of the project directory we can execute for starting it:

```
docker-compose up -d
```

Later, when we finish testing we have to do:

```
docker-compose down
```

Now we can use the console to manage the bucket and browse the content, etc:

http://localhost:9001
- user: minioadmin
- password: minioadmin

If we want to use the AWS cli tool we can create a profile for MinIO. For example in the $HOME/.aws/credentials
append the following:
```
[minio]
aws_access_key_id = minioadmin
aws_secret_access_key = minioadmin
```

# Getting Started

### Reference Documentation

For further reference, please consider the following sections:

* [Official Gradle documentation](https://docs.gradle.org)
* [Spring Boot Gradle Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/3.0.4/gradle-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/3.0.4/gradle-plugin/reference/html/#build-image)
* [Spring Web](https://docs.spring.io/spring-boot/docs/3.0.4/reference/htmlsingle/#web)
* [Spring Boot Actuator](https://docs.spring.io/spring-boot/docs/3.0.4/reference/htmlsingle/#actuator)
* [Spring Data JDBC](https://docs.spring.io/spring-boot/docs/3.0.4/reference/htmlsingle/#data.sql.jdbc)
* [Spring Boot DevTools](https://docs.spring.io/spring-boot/docs/3.0.4/reference/htmlsingle/#using.devtools)
* [Spring Configuration Processor](https://docs.spring.io/spring-boot/docs/3.0.4/reference/htmlsingle/#appendix.configuration-metadata.annotation-processor)
* [Liquibase Migration](https://docs.spring.io/spring-boot/docs/3.0.4/reference/htmlsingle/#howto.data-initialization.migration-tool.liquibase)

### Guides

The following guides illustrate how to use some features concretely:

* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/rest/)
* [Building a RESTful Web Service with Spring Boot Actuator](https://spring.io/guides/gs/actuator-service/)
* [Using Spring Data JDBC](https://github.com/spring-projects/spring-data-examples/tree/master/jdbc/basics)

### Additional Links

These additional references should also help you:

* [Gradle Build Scans – insights for your project's build](https://scans.gradle.com#gradle)

